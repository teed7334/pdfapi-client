<?php
require __DIR__ . '/vendor/autoload.php';

use teed7334\PDFApiClient\dto\createDto;
use teed7334\PDFApiClient\dto\signDto;
use teed7334\PDFApiClient\dto\readSignDto;
use teed7334\PDFApiClient\dto\uploadDto;

use teed7334\PDFApiClient\api\create;
use teed7334\PDFApiClient\api\sign;
use teed7334\PDFApiClient\api\readSign;
use teed7334\PDFApiClient\api\upload;

class main
{
    private $newPDF;
    private $signPDF;

    public function run()
    {
        $this->newPDF = $this->createPDF();
        $this->signPDF = $this->signPDF();
        $this->readSignPDF();
        $this->uploadPDF();
    }

    private function createPDF() : string
    {
        $api = new create;
        $dto = $this->bindCreateParams();
        $ro = $api->getResponse($dto);
        echo "新增PDF<br />HTTP STATUS: {$ro->code}<br />HTTP Body: {$ro->message}<br /><br /><br />";
        $message = json_decode($ro->message);
        return $message->message;
    }

    private function signPDF() : string
    {
        $api = new sign;
        $dto = $this->bindSignParams();
        $ro = $api->getResponse($dto);
        echo "加簽PDF<br />HTTP STATUS: {$ro->code}<br />HTTP Body: {$ro->message}<br /><br /><br />";
        $message = json_decode($ro->message);
        return $message->message;
    }

    private function readSignPDF()
    {
        $api = new readSign;
        $dto = $this->bindReadSignParams();
        $ro = $api->getResponse($dto);
        echo "加簽PDF資訊<br />HTTP STATUS: {$ro->code}<br />HTTP Body: {$ro->message}<br /><br /><br />";
    }

    private function uploadPDF()
    {
        $api = new upload;
        $dto = $this->bindUploadParams();
        $ro = $api->getResponse($dto);
        echo "上鏈PDF資訊<br />HTTP STATUS: {$ro->code}<br />HTTP Body: {$ro->message}<br /><br /><br />";
    }

    private function bindUploadParams() : uploadDto
    {
        $dto = new uploadDto;
        $dto->src = $this->signPDF;
        return $dto;
    }

    private function bindReadSignParams() : readSignDto
    {
        $dto = new readSignDto;
        $dto->src = $this->signPDF;
        return $dto;
    }

    private function bindSignParams() : signDto
    {
        $dto = new signDto;
        $dto->src = $this->newPDF;
        $dto->x = 0;
        $dto->y = 0;
        $dto->width = 0;
        $dto->height = 0;
        return $dto;
    }

    private function bindCreateParams() : createDto
    {
        $dto = new createDto;
        $dto->content = [
            'Peter: 用力測試',
            'Peter: 認真測試',
            'Peter: 盡力測試'
        ];
        return $dto;
    }
}

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$main = new main;
$main->run();
