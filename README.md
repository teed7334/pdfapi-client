# pdfapi-client

PDFAPI 串接程式

## 資料夾結構

app/api 串接API相關Client

app/dto 資料傳輸物件

app/helper 程式小幫手

index.php Demo運行用主程式

## 初始化

1. 安裝Composer
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e5325b19b381bfd88ce90a5ddb7823406b2a38cff6bb704b0acc289a09c8128d4a8ce2bbafcd1fcbdc38666422fe2806') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

2. 安裝相依套件
```
php composer.phar install
php composer.phar fund
```

3. 將.env.swp更名為.env

4. 於.env的url中，輸入API主機所在的位置，好比
```
http://127.0.0.1:8081
```

## 運行方式
1. 運行Web Server
```
php -S localhost:10080
```

2. 開啟瀏覽器，網址如下
```
http://localhost:10080
```