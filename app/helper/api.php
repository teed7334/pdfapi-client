<?php
namespace teed7334\PDFApiClient\helper;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

trait api
{
    public function call(string $url, $params) : Response
    {
        $client = new Client;
        $headers = ['Accept' => 'application/json'];
        $res = $client->post($url, [
            'headers' => $headers,
            'json' => $params,
        ]);
        return $res;
    }
}
