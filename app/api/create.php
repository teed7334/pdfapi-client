<?php
namespace teed7334\PDFApiClient\api;

use teed7334\PDFApiClient\helper\api;

use teed7334\PDFApiClient\dto\resultObject;
use teed7334\PDFApiClient\dto\createDto;

class create
{
    use api;
    
    public function getResponse(createDto $dto) : resultObject
    {
        $url = "{$_ENV['url']}/api/v1/create";
        $res = $this->call($url, $dto);
        $ro = new resultObject;
        $ro->code = $res->getStatusCode();
        $ro->message = $res->getBody()->getContents();
        return $ro;
    }
}
