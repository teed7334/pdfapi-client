<?php
namespace teed7334\PDFApiClient\api;

use teed7334\PDFApiClient\helper\api;

use teed7334\PDFApiClient\dto\resultObject;
use teed7334\PDFApiClient\dto\signDto;

class sign
{
    use api;

    public function getResponse(signDto $dto) : resultObject
    {
        $url = "{$_ENV['url']}/api/v1/sign";
        $res = $this->call($url, $dto);
        $ro = new resultObject;
        $ro->code = $res->getStatusCode();
        $ro->message = $res->getBody()->getContents();
        return $ro;
    }
}
