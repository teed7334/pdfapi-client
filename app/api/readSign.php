<?php
namespace teed7334\PDFApiClient\api;

use teed7334\PDFApiClient\helper\api;

use teed7334\PDFApiClient\dto\resultObject;
use teed7334\PDFApiClient\dto\readSignDto;

class readSign
{
    use api;

    public function getResponse(readSignDto $dto) : resultObject
    {
        $url = "{$_ENV['url']}/api/v1/readSign";
        $res = $this->call($url, $dto);
        $ro = new resultObject;
        $ro->code = $res->getStatusCode();
        $ro->message = $res->getBody()->getContents();
        return $ro;
    }
}
